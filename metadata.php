<?php
/**
 * Metadata version
 */
$sMetadataVersion = '2.0';

/**
 * Module information
 */
$aModule = array(
    'id' => 'dre_xmlsync',
    'title' => '<img src="../modules/bender/dre_xmlsync/out/img/favicon.ico" title="Bodynova XML Sync">odynova XML Sync nach Hause',
    'description' => [
        'de' => 'Bodynova XML sync nach Hause',
        'en' => 'Bodynova xml sync @ home'
    ],
    'version' => '1.5.0',
    'author' => 'André Bender',
    'url' => 'https://bodynova.de',
    'thumbnail' => 'out/img/logo_bodynova.png',
    'email' => 'support@bodynova.de',
    'controllers' => array(
    ),
    'extend' => array(
        \OxidEsales\Eshop\Application\Model\Order::class => \Bender\dre_xmlsync\models\dre_order::class,
    ),
    'blocks' => array(
    ),
    'templates' => array(
    ),
    'events' => array(
    ),
);
