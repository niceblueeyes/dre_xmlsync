<?xml version="1.0" encoding="utf-8" ?>

<!-- The one-fits-all XSLT for FileMaker.

     Transforms arbitrary XML grammars and namespaces
     into FileMaker records. Every node or attribute one
     record.

     Usage: Import your XML data with grammar FMPXMLRESULT using this file as
     XSLT stylesheet into a new FM table.

     Jens Teich v-0.4 12.08.2013
-->


<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.filemaker.com/fmpxmlresult">

    <xsl:template match="/">
        <FMPXMLRESULT>

            <ERRORCODE>0</ERRORCODE>
            <PRODUCT BUILD="" NAME="" VERSION=""/>
            <DATABASE DATEFORMAT="M/d/yyyy" LAYOUT="" NAME="oxorderarticles" RECORDS="1" TIMEFORMAT="h:mm:ss a"/>

            <METADATA>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXID"          TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXORDERID"     TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXAMOUNT"      TYPE="NUMBER"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXARTID"       TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXARTNUM"      TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXTITLE"       TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXSHORTDESC"   TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXSELVARIANT"  TYPE="TEXT"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXNETPRICE"    TYPE="NUMBER"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXBRUTPRICE"   TYPE="NUMBER"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXVATPRICE"    TYPE="NUMBER"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXVAT"         TYPE="NUMBER"/>
                <!--<FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXPERSPARAM"   TYPE="TEXT"/>-->
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXPRICE"       TYPE="NUMBER"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXBPRICE"      TYPE="NUMBER"/>
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXNPRICE"      TYPE="NUMBER"/>
                <!--<FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXPIC1"        TYPE="TEXT"/>
                #<FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXWEIGHT"      TYPE="NUMBER"/>
                #<FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXSTOCK"       TYPE="NUMBER"/>
                #<FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXDELIVERY"    TYPE="TEXT"/>
                #<FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXINSERT"      TYPE="TEXT"/>-->
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXTIMESTAMP"   TYPE="DATE"/>
                <!--<FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXLENGTH"      TYPE="NUMBER"/>
                #<FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXWIDTH"       TYPE="NUMBER"/>
                #<FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXHEIGHT"      TYPE="NUMBER"/>
                #<FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXFILE"        TYPE="TEXT"/>
                #<FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXSEARCHKEYS"  TYPE="TEXT"/>
                #<FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXTEMPLATE"    TYPE="TEXT"/>-->
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXFOLDER"      TYPE="TEXT"/>
                <!--<FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXSTORNO"      TYPE="NUMBER"/>
                #<FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXORDERSHOPID" TYPE="TEXT"/>-->
                <FIELD EMPTYOK="YES" MAXREPEAT="1" NAME="OXSHOP"      TYPE="TEXT"/>
            </METADATA>

            <RESULTSET FOUND="">
                <xsl:for-each select="/ROW/ORDER">
                    <ROW MODID="0" xmlns="http://www.filemaker.com/fmpxmlresult">
                        <COL><DATA><xsl:value-of select="OXID"/></DATA></COL>
                        <COL><DATA><xsl:value-of select="OXORDERID"/></DATA></COL>
                        <COL><DATA><xsl:value-of select="OXAMOUNT"/></DATA></COL>
                        <COL><DATA><xsl:value-of select="OXARTID"/></DATA></COL>
                        <COL><DATA><xsl:value-of select="OXARTNUM"/></DATA></COL>
                        <COL><DATA><xsl:value-of select="OXTITLE"/></DATA></COL>
                        <COL><DATA><xsl:value-of select="OXSHORTDESC"/></DATA></COL>
                        <COL><DATA><xsl:value-of select="OXSELVARIANT"/></DATA></COL>
                        <COL><DATA><xsl:value-of select="OXNETPRICE"/></DATA></COL>
                        <COL><DATA><xsl:value-of select="OXBRUTPRICE"/></DATA></COL>
                        <COL><DATA><xsl:value-of select="OXVATPRICE"/></DATA></COL>
                        <COL><DATA><xsl:value-of select="OXVAT"/></DATA></COL>
                        <!--<COL><DATA><xsl:value-of select="OXPERSPARAM"/></DATA></COL>-->
                        <COL><DATA><xsl:value-of select="OXPRICE"/></DATA></COL>
                        <COL><DATA><xsl:value-of select="OXBPRICE"/></DATA></COL>
                        <COL><DATA><xsl:value-of select="OXNPRICE"/></DATA></COL>
                        <!--<COL><DATA><xsl:value-of select="OXPIC1"/></DATA></COL>
                        <COL><DATA><xsl:value-of select="OXWEIGHT"/></DATA></COL>
                        <COL><DATA><xsl:value-of select="OXSTOCK"/></DATA></COL>
                        <COL><DATA><xsl:value-of select="OXDELIVERY"/></DATA></COL>
                        <COL><DATA><xsl:value-of select="OXINSERT"/></DATA></COL>-->
                        <COL><DATA><xsl:value-of select="OXTIMESTAMP"/></DATA></COL>
                        <!--<COL><DATA><xsl:value-of select="OXLENGTH"/></DATA></COL>
                        <COL><DATA><xsl:value-of select="OXWIDTH"/></DATA></COL>
                        <COL><DATA><xsl:value-of select="OXHEIGHT"/></DATA></COL>
                        <COL><DATA><xsl:value-of select="OXFILE"/></DATA></COL>
                        <COL><DATA><xsl:value-of select="OXSEARCHKEYS"/></DATA></COL>
                        <COL><DATA><xsl:value-of select="OXTEMPLATE"/></DATA></COL>-->
                        <COL><DATA><xsl:value-of select="OXFOLDER"/></DATA></COL>
                        <!--<COL><DATA><xsl:value-of select="OXSTORNO"/></DATA></COL>
                        <COL><DATA><xsl:value-of select="OXORDERSHOPID"/></DATA></COL>-->
                        <COL><DATA><xsl:value-of select="OXSHOP"/></DATA></COL>
                    </ROW>
                </xsl:for-each>
            </RESULTSET>
        </FMPXMLRESULT>
    </xsl:template>
</xsl:stylesheet>
