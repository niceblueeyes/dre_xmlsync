<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 21.07.16
 * Time: 13:27
 */
require dirname(__DIR__).'/core/array2xml.php';
require dirname(__DIR__).'/core/orderarticles2xml.php';

class dre_oxorderxml extends dre_oxorderxml_parent
{
	/**
	 * Class constructor, initiates parent constructor (parent::oxBase()).
	 */
	public function __construct()
	{
		parent::__construct();
		//$this->init('oxorder');
		// set usage of separate orders numbering for different shops
		//$this->setSeparateNumbering($this->getConfig()->getConfigParam('blSeparateNumbering'));
	}
	
    /**
     * Das OxOrderObjekt
     * @var
     */
    protected $oOrder;

    /**
     * Ein Array der einzelnen OxOrder Felder
     * @var array
     */
    protected $arOrderArray = array();

    /**
     * Getter für das OxOrder Objekt
     * @return mixed
     */
    public function getOrder(){
        return $this->oOrder;
    }

    /**
     * Setter für das OxOrderObjekt
     * @param $object
     */
    public function setOrder($object){
        if(isset($object)){
            $this->oOrder = $object;
        }
    }

    /**
     * Getter für das OxOrderArray
     * @return array
     */
    public function getOrderArray(){
        return $this->arOrderArray;
    }

    /**
     * Setter für das OxOrderArray, erwartet ein OxOrder Objekt als Parameter
     * @param $object
     */
    public function setOrderArray($object){
        if(isset($object)){
            $arOrderArray = $object;
        } else {
            echo 'Kein Objekt übergeben';
            return;
        }

        $array = array('ORDER' => array(
            'OXID'                  =>$arOrderArray->oxorder__oxid->value,
            'OXSHOPID'              =>'oxbaseshop',
            'OXUSERID'              =>$arOrderArray->oxorder__oxuserid->value,
            'OXCUSTNR'              =>'',
            'OXORDERDATE'           =>date_format(date_create($arOrderArray->oxorder__oxorderdate->value), 'Y-m-d H:i:s'),
            'OXORDERNR'             =>$arOrderArray->oxorder__oxordernr->value,
            'OXBILLCOMPANY'         =>$arOrderArray->oxorder__oxbillcompany->value,
            'OXBILLEMAIL'           =>$arOrderArray->oxorder__oxbillemail->value,
            'OXBILLFNAME'           =>$arOrderArray->oxorder__oxbillfname->value,
            'OXBILLLNAME'           =>$arOrderArray->oxorder__oxbilllname->value,
            'OXBILLSTREET'          =>$arOrderArray->oxorder__oxbillstreet->value,
            'OXBILLSTREETNR'        =>$arOrderArray->oxorder__oxbillstreetnr->value,
            'OXBILLADDINFO'         =>$arOrderArray->oxorder__oxbilladdinfo->value,
            'OXBILLUSTID'           =>$arOrderArray->oxorder__oxbillustid->value,
            'OXBILLCITY'            =>$arOrderArray->oxorder__oxbillcity->value,
            'OXBILLCOUNTRYID'       =>$arOrderArray->oxorder__oxbillcountryid->value,
            'OXBILLCOUNTRYTITLE'    =>'',
            'OXBILLISOALPHA2'       =>'',
            'OXBILLISOALPHA3'       =>'',
            'OXBILLSTATEID'         =>$arOrderArray->oxorder__oxbillstateid->value,
            'OXBILLZIP'             =>$arOrderArray->oxorder__oxbillzip->value,
            'OXBILLFON'             =>$arOrderArray->oxorder__oxbillfon->value,
            'OXBILLFAX'             =>$arOrderArray->oxorder__oxbillfax->value,
            'OXBILLSAL'             =>$arOrderArray->oxorder__oxbillsal->value,
            'OXDELCOMPANY'          =>$arOrderArray->oxorder__oxdelcompany->value,
            'OXDELFNAME'            =>$arOrderArray->oxorder__oxdelfname->value,
            'OXDELLNAME'            =>$arOrderArray->oxorder__oxdellname->value,
            'OXDELSTREET'           =>$arOrderArray->oxorder__oxdelstreet->value,
            'OXDELSTREETNR'         =>$arOrderArray->oxorder__oxdelstreetnr->value,
            'OXDELADDINFO'          =>$arOrderArray->oxorder__oxdeladdinfo->value,
            'OXDELCITY'             =>$arOrderArray->oxorder__oxdelcity->value,
            'OXDELCOUNTRYID'        =>$arOrderArray->oxorder__oxdelcountryid->value,
            'OXDELCOUNTRYTITLE'     =>'',
            'OXDELISOALPHA2'        =>'',
            'OXDELISOALPHA3'        =>'',
            'OXDELSTATEID'          =>$arOrderArray->oxorder__oxdelstateid->value,
            'OXDELZIP'              =>$arOrderArray->oxorder__oxdelzip->value,
            'OXDELFON'              =>$arOrderArray->oxorder__oxdelfon->value,
            'OXDELFAX'              =>$arOrderArray->oxorder__oxdelfax->value,
            'OXDELSAL'              =>$arOrderArray->oxorder__oxdelsal->value,
            'OXPAYMENTID'           =>$arOrderArray->oxorder__oxpaymentid->value,
            'OXPAYMENTTYPE'         =>$arOrderArray->oxorder__oxpaymenttype->value,
            'OXTOTALNETSUM'         =>str_replace('.',',',round($arOrderArray->oxorder__oxtotalnetsum->value, 2)),
            'OXTOTALBRUTSUM'        =>str_replace('.',',',round($arOrderArray->oxorder__oxtotalbrutsum->value, 2)),
            'OXTOTALORDERSUM'       =>str_replace('.',',',round($arOrderArray->oxorder__oxtotalordersum->value, 2)),
            'OXARTVAT1'             =>str_replace('.',',',round($arOrderArray->oxorder__oxartvat1->value, 2)),
            'OXARTVATPRICE1'        =>str_replace('.',',',round($arOrderArray->oxorder__oxartvatprice1->value, 2)),
            'OXARTVAT2'             =>str_replace('.',',',round($arOrderArray->oxorder__oxartvat2->value, 2)),
            'OXARTVATPRICE2'        =>str_replace('.',',',round($arOrderArray->oxorder__oxartvatprice2->value, 2)),
            'OXDELCOST'             =>str_replace('.',',',round($arOrderArray->oxorder__oxdelcost->value, 2)),
            'OXDELVAT'              =>str_replace('.',',',round($arOrderArray->oxorder__oxdelvat->value, 2)),
            'OXPAYCOST'             =>str_replace('.',',',round($arOrderArray->oxorder__oxpaycost->value, 2)),
            'OXPAYVAT'              =>str_replace('.',',',round($arOrderArray->oxorder__oxpayvat->value, 2)),
            /*
            'OXWRAPCOST'            =>$arOrderArray->oxorder__oxwrapcost->value,
            'OXWRAPVAT'             =>$arOrderArray->oxorder__oxwrapvat->value,
            'OXGIFTCARDCOST'        =>$arOrderArray->oxorder__oxgiftcardcost->value,
            'OXGIFTCARDVAT'         =>$arOrderArray->oxorder__oxgiftcardvat->value,
            'OXCARDID'              =>$arOrderArray->oxorder__oxcardid->value,
            'OXCARDTEXT'            =>$arOrderArray->oxorder__oxcardtext->value,
            */
            'OXDISCOUNT'            =>str_replace('.',',',round($arOrderArray->oxorder__oxdiscount->value, 2)),
            /*
            'OXEXPORT'              =>$arOrderArray->oxorder__oxexport->value,
            'OXBILLNR'              =>$arOrderArray->oxorder__oxbillnr->value,
            'OXBILLDATE'            =>$arOrderArray->oxorder__oxbilldate->value, //date_format(date_create($arOrderArray->oxorder__oxbilldate->value), 'Y-m-d H:i:s'), //$arOrderArray->oxorder__oxbilldate->value,
            */
            'OXTRACKCODE'           =>$arOrderArray->oxorder__oxtrackcode->value,
            'OXSENDDATE'            =>$arOrderArray->oxorder__oxsenddate->value, //date_format(date_create($arOrderArray->oxorder__oxsenddate->value), 'Y-m-d H:i:s'), //$arOrderArray->oxorder__oxsenddate->value,
            'OXREMARK'              =>$arOrderArray->oxorder__oxremark->value,
            'OXVOUCHERDISCOUNT'     =>str_replace('.',',',round($arOrderArray->oxorder__oxvoucherdiscount->value, 2)),
            /*
            'OXCURRENCY'            =>$arOrderArray->oxorder__oxcurrency->value,
            'OXCURRATE'             =>$arOrderArray->oxorder__oxcurrate->value,
            */
            'OXFOLDER'              =>$arOrderArray->oxorder__oxfolder->value,
            'OXTRANSID'             =>$arOrderArray->oxorder__oxtransid->value,
            'OXPAYID'               =>$arOrderArray->oxorder__oxpayid->value,
            'OXXID'                 =>$arOrderArray->oxorder__oxxid->value,
            'OXPAID'                =>$arOrderArray->oxorder__oxpaid->value,
            'OXSTORNO'              =>$arOrderArray->oxorder__oxstorno->value,
            //'OXIP'                  =>$arOrderArray->oxorder__oxip->value,
            'OXTRANSSTATUS'         =>$arOrderArray->oxorder__oxtransstatus->value,
            'OXLANG'                =>$arOrderArray->oxorder__oxlang->value,
            /*
            'OXINVOICENR'           =>$arOrderArray->oxorder__oxinvoicenr->value,
            'OXDELTYPE'             =>$arOrderArray->oxorder__oxdeltype->value,
            'OXTSPROTECTID'         =>$arOrderArray->oxorder__oxtsprotectid->value,
            'OXTSPROTECTCOSTS'      =>$arOrderArray->oxorder__oxtsprotectcosts->value,
            */
            'OXTIMESTAMP'           =>date_format(date_create($arOrderArray->oxorder__oxtimestamp->value), 'Y-m-d H:i:s'), //$arOrderArray->oxorder__oxtimestamp->value,
            'OXISNETTOMODE'         =>$arOrderArray->oxorder__oxisnettomode->value,
            'oxefiorderchecksum'    =>$arOrderArray->oxorder__oxefiorderchecksum->value,
            'oxefiaddchecksum'      =>$arOrderArray->oxorder__oxefiaddchecksum->value,
            'oxefiorderartschecksum'=>$arOrderArray->oxorder__oxefiorderartschecksum->value,
            'flagversand'           =>$arOrderArray->oxorder__flagversand->value,
            'Konstante_Lieferschein'=>'Lieferschein',
            'bankdatenexport'       =>$arOrderArray->oxorder__bankdatenexport->value,
            'oxshop'                =>'endkunden',
            'iswhitelabel'          =>'',
            'AuthCode'              =>'',
            'lsbankname'            =>'',
            'lsblz'                 =>'',
            'lsktonr'               =>'',
            'lsktoinhaber'          =>'',
            'oxdateused'            =>'',
            'oxreserved'            =>'',
            'oxvouchernr'           =>'',
            'oxvoucherserieid'      =>'',
            'oxvoucherserie'        =>'',
            'oxvoucherseriedescription'=>'',
            'oxvoucherseriediscount'=>'',
            'oxvoucherseriediscounttyp'=>'',
            'oxdiscount'            =>'',
            'oxid'                  =>'',
            'oxtimestamp'           =>'',
            'usedDiscountsTitle'    =>$arOrderArray->oxorder__useddiscountstitle->value,
            'usedDiscountsValue'    =>str_replace('.',',',round($arOrderArray->oxorder__useddiscountsvalue->value, 2)),
        ));

        $this->arOrderArray[0] = $array;
    }

    /**
     * Holt die Kundennummer aus der Datenbank und schreibt sie in das OxOrderArray
     */
    public function setCustNr(){
        $UserSelect = "select oxcustnr from oxuser where OXID = '".$this->arOrderArray[0]['ORDER']['OXUSERID']."'";
        $Usernr = oxDb::getDb()->getOne($UserSelect);
        #echo '<h1>USERNR = '.$UserSelect.'</h1>';
        $this->arOrderArray[0]['ORDER']['OXCUSTNR'] = $Usernr;
    }

    /**
     * Gibt die Kundennummer aus dem OxOrderArray aus.
     * @return mixed
     */
    public function getCustNr(){
        return $this->arOrderArray[0]['ORDER']['OXCUSTNR'];
    }

    /**
     * Holt das Rechnungsland aus der Datenbank und setzt es in das OxOrderArray
     */
    public function setBillCountry(){
        $countryid = $this->arOrderArray[0]['ORDER']['OXBILLCOUNTRYID'];
        $Billcountryselect = "select oxid from oxcountry where OXID = '".$countryid."'";
        $Billcountryid     = oxDb::getDb()->getOne($Billcountryselect);
        $oBillCountry      = oxNew("oxcountry");
        $oBillCountry ->load($Billcountryid);

        $this->arOrderArray[0]['ORDER']['OXBILLCOUNTRYTITLE']     = $oBillCountry->oxcountry__oxtitle->value;
        $this->arOrderArray[0]['ORDER']['OXBILLISOALPHA2']        = $oBillCountry->oxcountry__oxisoalpha2->value;
        $this->arOrderArray[0]['ORDER']['OXBILLISOALPHA3']        = $oBillCountry->oxcountry__oxisoalpha3->value;
    }

    /**
     * Holt das Lieferland aus der Datenbank und setzt sie in das OxOrderArray
     */
    public function setDeliveryCountry(){
        $delCountryid = $this->arOrderArray[0]['ORDER']['OXDELCOUNTRYID'];
        if(isset($delCountryid)){
            $Delcountryselect = "select oxid from oxcountry where OXID = '".$delCountryid."'";
            $Delcountryid     = oxDb::getDb()->getOne($Delcountryselect);
            $oDelCountry      = oxNew("oxcountry");
            $oDelCountry->load($Delcountryid);

            $this->arOrderArray[0]['ORDER']['OXDELCOUNTRYTITLE']  = $oDelCountry->oxcountry__oxtitle->value;
            $this->arOrderArray[0]['ORDER']['OXDELISOALPHA2']     = $oDelCountry->oxcountry__oxisoalpha2->value;
            $this->arOrderArray[0]['ORDER']['OXDELISOALPHA3']     = $oDelCountry->oxcountry__oxisoalpha3->value;
        }
    }

    /**
     * Verarbeitet die Oxpayments und setzt sie in das OxOrderArray
     */
    public function setPayments(){
        // OXUSERPAYMENTS

        // Kreditkarte
        if ($this->arOrderArray[0]['ORDER']['OXPAYMENTTYPE'] === 'ipaymentcw_mastercard' || $this->arOrderArray[0]['ORDER']['OXPAYMENTTYPE'] === 'ipaymentcw_visa') {
            $ipaymentselect = "select authorizationStatus from ipaymentcw_transaction where orderId = '" . $this->arOrderArray[0]['ORDER']['OXID'] . "'";
            $ipaymentAuthCode = DatabaseProvider::getDb()->getOne($ipaymentselect);
            $this->arOrderArray[0]['ORDER']['AuthCode'] = $ipaymentAuthCode;

            // transid setzen
            $ipaymenttransselect = "select paymentid from ipaymentcw_transaction where orderId = '" . $this->arOrderArray[0]['ORDER']['OXID'] . "'";
            $ipaymenttansid = DatabaseProvider::getDb()->getOne($ipaymenttransselect);
            $this->arOrderArray[0]['ORDER']['OXTRANSID'] = $ipaymenttansid;

        } else {
            $this->arOrderArray[0]['ORDER']['AuthCode'] = '';
        }

        // Lastschrift
        if($this->arOrderArray[0]['ORDER']['OXPAYMENTTYPE'] === 'oxiddebitnote'){
            $oPayment = oxNew("oxuserpayment");
            $oPayment->load($this->arOrderArray[0]['ORDER']['OXPAYMENTID']);
            $dyn = $oPayment->getDynValues();
            foreach($dyn as $key){
                $this->arOrderArray[0]['ORDER'][$key->name] = $key->value;
            }
        }else{
            $this->arOrderArray[0]['ORDER']['lsbankname']   = '';
            $this->arOrderArray[0]['ORDER']['lsblz']        = '';
            $this->arOrderArray[0]['ORDER']['lsktonr']      = '';
            $this->arOrderArray[0]['ORDER']['lsktoinhaber'] = '';
        }
    }

    /**
     * Verarbeitet die Vouchers und setzt sie in das OxOrder Array
     */
    public function setVouchers(){
        $Voucherselect      = "select * from oxvouchers where OXORDERID = '".$this->arOrderArray[0]['ORDER']['OXID']."'";
        $Voucher            = oxDb::getDb()->getArray($Voucherselect);

        if(isset($Voucher[0])) {
            $this->arOrderArray[0]['ORDER']['oxdateused'] = $Voucher[0][0];
            $this->arOrderArray[0]['ORDER']['oxreserved'] = $Voucher[0][3];
            $this->arOrderArray[0]['ORDER']['oxvouchernr'] = $Voucher[0][4];
            $this->arOrderArray[0]['ORDER']['oxvoucherserieid'] = $Voucher[0][5];
            $this->arOrderArray[0]['ORDER']['oxdiscount'] = str_replace('.', ',', round($Voucher[0][6], 2));
            $this->arOrderArray[0]['ORDER']['oxid'] = $Voucher[0][7];
            $this->arOrderArray[0]['ORDER']['oxtimestamp'] = $Voucher[0][8];

            $Voucherserieselect = "select * from oxvoucherseries where OXID ='" . $Voucher[0][5] . "'";
            $Voucherserie = oxDb::getDb()->getArray($Voucherserieselect);

            $this->arOrderArray[0]['ORDER']['oxvoucherserie'] = $Voucherserie[0][2];
            $this->arOrderArray[0]['ORDER']['oxvoucherseriedescription'] = $Voucherserie[0][3];
            $this->arOrderArray[0]['ORDER']['oxvoucherseriediscount'] = $Voucherserie[0][4];
            $this->arOrderArray[0]['ORDER']['oxvoucherseriediscounttyp'] = $Voucherserie[0][5];
        }
    }

    /**
     * erstellt eine xml-Datei mit der Ordernr als Dateiname und speichert sie im Ordner ./Orderdaten ab.
     */
    public function erstelleOrderXmlDatei(){

        $xml = null;
        try{
            $null = null;
            $id = 0;
            $xml = new array2xml('ROW');
            $arAlleOrders = $this->getOrderArray();
            $anzahlAlleOrders = count($arAlleOrders);
            foreach($arAlleOrders as $keyarray){

                $xml->createNode( $keyarray, $null, $anzahlAlleOrders ,$id);
                $id++;
            }
            $fp = fopen( dirname(__DIR__).'/OrderDaten/'.$this->oxorder__oxordernr->value.'.xml', 'w');
            fwrite($fp, $xml);
            fclose($fp);
        }catch (Exception $e)
        {
            #echo $e->getMessage();
            $oxEmail = oxNew('oxemail');
            $oxEmail->setSubject('BODYNOVA FEHLER');
            $oxEmail->setBody("Error in Datei: dre_oxorderxml.php function: erstelleOrderXmlDatei()\n".
                "Geworfener Fehler: ".$e->getMessage());
            $oxEmail->setRecipient('a.bender@bodynova.de', 'André Bender');
            $oxEmail->setReplyTo('server@bodynova.de' , 'Server Bodynova');
            $oxEmail->send();
        }


    }

    public function ftpUploadFritzBox(){

        try {
            // FTP Upload auf fritz.nas
            $ftp_server = '92.50.75.170';
            $ftp_user = 'haendlershop';
            $ftp_pass = 'peanuts30';
            #$file = dirname(__FILE__).'/'.barcode_outimage($code,getvar('encoding'),getvar('scale'),getvar('mode'));
            #$file = $bild;
            $file = dirname(__DIR__).'/OrderDaten/' . $this->oxorder__oxordernr->value . '.xml';
            $destination_file = 'JetFlash-Transcend16GB-01/FRITZ/OrderImport/' . $this->oxorder__oxordernr->value . '.xml';
            $cid = ftp_connect($ftp_server);
            ftp_login($cid, $ftp_user, $ftp_pass);
            ftp_pasv($cid, true);
            ftp_put($cid, $destination_file, $file, FTP_BINARY);
            ftp_close($cid);
        } catch (Exception $e){
            #echo $e->getMessage();
            $oxEmail = oxNew('oxemail');
            $oxEmail->setSubject('BODYNOVA FEHLER');
            $oxEmail->setBody("Error in Datei: dre_oxorderxml.php function: ftpUploadFritzBox()\n".
                "Geworfener Fehler: ".$e->getMessage());
            $oxEmail->setRecipient('a.bender@bodynova.de', 'André Bender');
            $oxEmail->setReplyTo('server@bodynova.de' , 'Server Bodynova');
            $oxEmail->send();
        }
    }

    public function finalizeOrder(oxBasket $oBasket, $oUser, $blRecalculatingOrder = false)
    {
        $iret = parent::finalizeOrder($oBasket, $oUser, $blRecalculatingOrder = false);

	    if($oUser->inGroup('5492a780bbf7c5c7a898b4bbbc9c4efc')){
			$oUser->removeFromGroup('5492a780bbf7c5c7a898b4bbbc9c4efc');
	    }
	   
	    
        if($iret == 1){
            if($this->oxorder__oxtransstatus->value === 'OK' || $this->oxorder__oxtransstatus->value === 'NOT_FINISHED'){
                $oOrder = $this;
                $oOrder->setOrderArray($oOrder);
                $oOrder->setCustNr();
                $oOrder->setBillCountry();
                $oOrder->setDeliveryCountry();
                $oOrder->setPayments();
                $oOrder->setVouchers();
                $oOrder->erstelleOrderXmlDatei();
                #$oOrder->ftpUploadFritzBox();

                $orderArticles = new orderarticles2xml();
                $orderArticles->setOrderId($oOrder->oxorder__oxid->value);
                $orderArticles->setOrderNr($oOrder->oxorder__oxordernr->value);
                $orderArticles->setOrderArticleIds();
                $orderArticles->setOrderArticles();
                $orderArticles->setOrderArticleXmlArray();
                $orderArticles->erstelleOrderArticlesXmlDatei();
                #$orderArticles->ftpUploadFritzBox();

                /*
                $oxEmail = oxNew('oxemail');
                $oxEmail->setBody('ORDER erstellt '.$this->oxorder__oxordernr->value."<br>".'OXTRANSSTAUS: '.$this->oxorder__oxtransstatus->value.'<br> OXPAYMENT: '.$this->oxorder__oxpaymenttype->value.'<br> $iret: '.$iret.'<br> OXTOTALBRUTSUM:'.$this->oxorder__oxtotalbrutsum->value);
                $oxEmail->setSubject('ORDER: '.$this->oxorder__oxordernr->value.' im Endkundenshop eingegangen');
                $oxEmail->setRecipient('order@bodynova.de', 'Bodynova GmbH');
                $oxEmail->setReplyTo('order@bodynova.de' , 'Bodynova GmbH');
                $oxEmail->send();
                */
            }
        }

        return $iret;
    }
}
